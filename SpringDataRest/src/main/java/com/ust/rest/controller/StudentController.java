package com.ust.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ust.rest.dto.CreateStudentRequest;
import com.ust.rest.dto.StudentDetails;
import com.ust.rest.entity.Student;
import com.ust.rest.service.StudentService;
import com.ust.rest.util.StudentUtil;

@RestController
@RequestMapping("/students")
@Validated
public class StudentController {
	@Autowired
	private StudentService sService;
	
	@Autowired
	private StudentUtil studentUtil;

	private static final Logger logger = LoggerFactory.getLogger(StudentController.class);

	@GetMapping
	public List<StudentDetails> fetchAll(){
		List<Student> students = sService.findAll();
		List<StudentDetails> details = studentUtil.toDetails(students);
		return details;
	}
	
	@GetMapping("/by/name/{name}")
	public List<StudentDetails> fetchStudentByName(@PathVariable("name") String name){
		System.out.println("find by name: "+name);
		List<Student> studList = sService.findByName(name);
//		List<StudentDetails> details = studentUtil.toDetails(studList);
//		return details;
		return studentUtil.toDetails(studList);
	}
	@GetMapping("/by/name/{fname}/{lname}")
	public List<StudentDetails> fetchStudentByFullName(@PathVariable("fname") String fname,
			@PathVariable("lname") String lname){
		System.out.println("find by name: "+fname+" "+lname);
		//List<Student> studList = sService.findByFullName(fname, lname);
		List<Student> studList = sService.findByFirstNameAndLastName(fname, lname);
		List<StudentDetails> details = studentUtil.toDetails(studList);
		return details;
	}
	
	@GetMapping("/by/id/{id}")
	public StudentDetails fetchStudent(@PathVariable("id") Integer id) {
		logger.info("Inside fetch controller id:"+id);
		System.out.println("find by id:"+id);
		Student student = sService.findById(id);
//		Student student = new Student(1234, "Rakhee", "Pisharody", 19, null);
//		Course course = new Course(2233, "Java", 5000, student);
//		Set<Course> courses = new HashSet<>();
//		courses.add(course);
//		student.setCourses(courses);
		StudentDetails details = studentUtil.toDetails(student);
		return details;
	}
	
	@PostMapping("/add")
	public StudentDetails addStudent(@RequestBody @Valid CreateStudentRequest requestData) {
		System.out.println("req data: " + requestData);
		Student stud = studentUtil.toStudent(requestData);
		System.out.println("stud: " + stud);
		Student regStud = sService.register(stud);
		StudentDetails details = studentUtil.toDetails(regStud);
		return details;
	}

}
