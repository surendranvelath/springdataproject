package com.ust.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CentralizedExceptionHandler {
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ExceptionHandler(StudentException.class)
	public String prodNotFound(StudentException e) {
		System.out.println("--in exception handler--");
		return e.getMessage();
	}
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(Exception.class)
	public String error(Exception e) {
		System.out.println("--in error handler--");
		return e.getMessage()+", please try after some time";
	}
}
