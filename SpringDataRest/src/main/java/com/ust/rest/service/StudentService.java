package com.ust.rest.service;

import java.util.List;

import com.ust.rest.entity.Student;

public interface StudentService {

	Student findById(Integer id);

	Student register(Student stud);

	List<Student> findByName(String name);

	List<Student> findAll();

	List<Student> findByFullName(String fname, String lname);

	List<Student> findByFirstNameAndLastName(String fname, String lname);


}
