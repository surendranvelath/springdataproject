package com.ust.rest.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.stereotype.Component;

import com.ust.rest.dto.CourseDetails;
import com.ust.rest.dto.CreateStudentRequest;
import com.ust.rest.dto.StudentDetails;
import com.ust.rest.entity.Course;
import com.ust.rest.entity.Student;

@Component
public class StudentUtil {

	public StudentDetails toDetails(Student student) {
		Set<Course> courses = student.getCourses();
		Set<CourseDetails> details = new HashSet<>();
		// to prevent cyclic reference to student in course when JSON is created
		for (Course course : courses) {
			details.add(new CourseDetails(course));
		}
		return new StudentDetails(student.getId(), student.getFirstName(), student.getLastName(), student.getAge(),
				details);
	}
	public List<StudentDetails> toDetails(List<Student> studList) {
		List<StudentDetails> details = new ArrayList<>();
		for (Student student : studList) {
			StudentDetails sd = toDetails(student);
			details.add(sd);
		}
		return details;
	}

	public Student toStudent(@Valid CreateStudentRequest requestData) {
		Student stud = new Student(requestData.getFirstName(), requestData.getLastName(), requestData.getAge());
		Set<Course> courses = requestData.getCourses();
		if (courses != null) {
			for (Course course : courses) {
				stud.addCourse(course);
			}
		}
		return stud;
	}


}
